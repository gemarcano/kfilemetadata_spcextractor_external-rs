// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2022

use ctr_cart::Cart as CTRCart;
use ctr_cart::SMDHRead;
use ctr_cart::CIA;
use gb_cart::licensee::Licensee;
use gb_cart::Cart as GBCart;
use gba_cart::Cart as GBACart;
use gcn_disk::Disk;
use n64_cart::Cart as N64Cart;
use nds_cart::Cart as NDSCart;
use rvz::HeaderRead;
use rvz::Rvz;
use serde::Deserialize;
use serde::Serialize;
use serde_with::skip_serializing_none;
use sfc_cart::Cart as SFCCart;
use spc_tag::id666::Emulator;
use spc_tag::xid666::XID666Subchunk;
use spc_tag::MetadataRead;

use std::fs::File;
use std::io;
use std::io::Read;
use std::io::Seek;
use std::str;

#[derive(Serialize, Deserialize)]
/// Represents input data from `KFileMetadata` for external plugins
struct Input {
    path: String,
    mimetype: String,
}

/// Represents the output data to `KFileMetadata` for external plugins
#[derive(Serialize, Deserialize)]
struct SPCOutput {
    status: String,
    properties: SPCProperties,
}

/// Represents the output data to `KFileMetadata` for external plugins
#[derive(Serialize, Deserialize)]
struct GameOutput {
    status: String,
    properties: GameProperties,
}

/// Properties that can be reported to `KFileMetadata` when parsing SPC files. This particular struct
/// has been tuned for SPC parsing purposes. The full list of properties `KFileMetadata` understands
/// is much larger.
#[skip_serializing_none]
#[derive(Default, Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct SPCProperties {
    artist: Option<String>,        // artist of song
    channels: i32,                 // Stereo, 2
    comment: Option<String>,       // Comments|Comments
    description: Option<String>,   // Game Title|Game Name, name of dumper|Dumper's name
    copyright: Option<String>,     // copyright year
    creation_date: Option<String>, // Date dumped|Date song was dumped
    duration: Option<u32>,         // combined total length
    generator: Option<String>,     // emulator used to dump|emulator used
    keywords: Option<String>,      // Game Title|Game Name, Super Nintendo, SPC700
    publisher: Option<String>,     // Publisher's name
    release_year: Option<i32>,     // copyright year
    sample_rate: i32,              // SNES, 32000
    title: Option<String>,         // Song Title|Song Name|Official soundtrack title
    track_number: Option<i32>,     // OST track
    disc_number: Option<i32>,      // OST disc
    compilation: Option<String>,   // Game Title?, Game Name priority
}

/// Properties that can be reported to KFileMetadata when parsing SFC files. This particular struct
/// has been tuned for SFC parsing purposes. The full list of properties KFileMetadata understands
/// is much larger.
#[skip_serializing_none]
#[derive(Default, Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct GameProperties {
    comment: Option<String>,       // Comments|Comments
    description: Option<String>,   // Game Title|Game Name, name of dumper|Dumper's name
    copyright: Option<String>,     // copyright year
    creation_date: Option<String>, // Date dumped|Date song was dumped
    duration: Option<u32>,         // combined total length
    generator: Option<String>,     // emulator used to dump|emulator used
    keywords: Option<String>,      // Game Title|Game Name, Super Nintendo, SPC700
    publisher: Option<String>,     // Publisher's name
    release_year: Option<i32>,     // copyright year
    title: Option<String>,         // Song Title|Song Name|Official soundtrack title
}

/// Trims null and whitespace characters (in that order) from the given string.
fn trim(string: &str) -> &str {
    string.trim_matches('\0').trim()
}

/// Helper function, returns either a string if it is not empty after a null and whitespace trim,
/// or None.
fn str_or_none(string: &str) -> Option<String> {
    let string = trim(string);
    (!string.is_empty()).then(|| string.to_string())
}

fn prepare_spc(input: &Input) {
    // Great, now we know which file we want, so access and parse it
    let mut file = File::open(&input.path).unwrap();
    let metadata = file.read_spc_metadata().unwrap();

    // Prepare properties for Output... almost all fields are optional
    let mut properties = SPCProperties {
        channels: 2,
        sample_rate: 32000,
        ..Default::default()
    };

    // Process ID666 first, if it exists. This is on purpose, as the XID666 fields, for this
    // implementation, take priority and will override ID666 ones later,
    if let Some(id666) = metadata.id666 {
        properties.artist = str_or_none(id666.get_song_artist());
        properties.comment = str_or_none(id666.get_comments());
        properties.description = str_or_none(id666.get_name_of_dumper());
        let duration = id666.get_seconds_until_fade() + id666.get_fade_length_ms() / 1000;
        if duration != 0 {
            properties.duration = Some(duration);
        }
        properties.generator = match id666.get_emulator_used_to_dump() {
            Emulator::Unknown(_) => None,
            emu => Some(format!("{emu}")),
        };

        properties.keywords = Some("Super Nintendo, SPC700".to_string());
        if !trim(id666.get_game_title()).is_empty() {
            properties.keywords = Some(format!(
                "Super Nintendo, SPC700, {}",
                trim(id666.get_game_title())
            ));
        }
        properties.title = str_or_none(id666.get_song_title());
        properties.compilation = str_or_none(id666.get_game_title());
    }

    // Now go through XID666 subchunks. Many of these might override properties set by ID666 fields
    // earlier.
    if let Some(xid666) = metadata.xid666 {
        let mut length: u32 = 0; // total song length (in 1/64000th of a second)
        let mut loop_length: u32 = 0; // part of total, how long is a loop (1/64000th of a second)
        let mut loop_amount: u32 = 1; // how many loops

        // Sentinel variable, help track if we run into an official title subchunk. This one takes
        // priority in setting the title over SongName and the ID666 song title field.
        let mut official_title = false;
        for subchunk in &xid666 {
            match subchunk {
                XID666Subchunk::DateDumped(date) => {
                    properties.creation_date = Some(date.format("%Y-%m-%d").to_string());
                }
                XID666Subchunk::PublisherName(name) => {
                    properties.publisher = Some(name.to_string());
                }
                XID666Subchunk::CopyrightYear(year) => {
                    properties.release_year = Some(i32::from(*year));
                }
                XID666Subchunk::TrackNumber(track) => {
                    properties.track_number = Some(i32::from(*track));
                }
                XID666Subchunk::DiscNumber(disc) => {
                    properties.track_number = Some(i32::from(*disc));
                }
                XID666Subchunk::Comments(comments) => {
                    properties.comment = Some(comments.to_string());
                }
                XID666Subchunk::GameName(name) => {
                    properties.compilation = Some(name.to_string());
                    properties.keywords =
                        Some(format!("Super Nintendo, Super Famicom, SPC700, {name}"));
                }
                XID666Subchunk::IntroLength(len)
                | XID666Subchunk::EndLength(len)
                | XID666Subchunk::FadeLength(len) => {
                    length += *len;
                }
                XID666Subchunk::LoopLength(len) => {
                    loop_length = *len;
                }
                XID666Subchunk::LoopAmount(amount) => {
                    loop_amount = u32::from(*amount);
                }
                XID666Subchunk::SongName(name) => {
                    if !official_title {
                        properties.title = Some(name.to_string());
                    }
                }
                XID666Subchunk::OfficialTitle(name) => {
                    official_title = true;
                    properties.title = Some(name.to_string());
                }
                XID666Subchunk::ArtistName(name) => {
                    properties.artist = Some(name.to_string());
                }
                _ => {}
            }
        }
        // Now, compute total length and only update it if it's not 0
        length += loop_amount * loop_length;
        if length > 0 {
            properties.duration = Some(length / 64000);
        }
    }

    let output = SPCOutput {
        status: "OK".to_string(),
        properties,
    };

    println!("{}", serde_json::to_string(&output).unwrap());
}

fn prepare_sfc(input: &Input) {
    // Great, now we know which file we want, so access and parse it
    let file = File::open(&input.path).unwrap();
    let cart = SFCCart::new(file);

    if cart.is_err() {
        return;
    }
    let mut cart = cart.unwrap();
    let crc = cart.compute_checksum();
    if crc.is_err() {
        return;
    }
    let crc = crc.unwrap();
    let header = cart.header;

    // Prepare properties for Output... almost all fields are optional
    let properties = GameProperties {
        title: Some(header.title.clone()),
        description: Some(format!(
            "Chipset: {}\n\
            Coprocessor: {}\n\
            Speed: {}\n\
            Mode: {}\n\
            ROM: {} KiB, RAM: {} KiB\n\
            Region: {}\n\
            Version {}\n\
            Checksum {}\n\
            Checksum pass: {}",
            header.chipset,
            header.coprocessor,
            header.speed,
            header.mode,
            header.rom_size,
            header.ram_size,
            header.region,
            header.version,
            header.checksum,
            header.checksum == crc,
        )),
        publisher: Some(format!("{}", header.dev_id)),
        keywords: Some(format!("Super Nintendo, Super Famicom, {}", header.title)),
        ..Default::default()
    };

    let output = GameOutput {
        status: "OK".to_string(),
        properties,
    };

    println!("{}", serde_json::to_string(&output).unwrap());
}

trait ReadSeek: std::io::Read + Seek {}
impl<T: Seek + Read> ReadSeek for T {}

fn prepare_gcn(input: &Input) {
    // Great, now we know which file we want, so access and parse it
    let mut file = File::open(&input.path).unwrap();
    let disk: &mut (dyn ReadSeek) = if file.has_rvz_magic() {
        let io = Rvz::new(file);
        &mut io.unwrap()
    } else {
        &mut file
    };
    let disk = Disk::new(disk);
    let header = disk.unwrap().header;

    // Prepare properties for Output... almost all fields are optional
    let properties = GameProperties {
        title: Some(header.game_name.clone()),
        description: Some(format!(
            "Game code: {}{}{}",
            header.console_id, header.game_code, header.country_code,
        )),
        publisher: Some(header.maker_code.to_string()),
        keywords: Some(format!("Gamecube, {}", header.game_name)),
        ..Default::default()
    };

    let output = GameOutput {
        status: "OK".to_string(),
        properties,
    };

    println!("{}", serde_json::to_string(&output).unwrap());
}

fn prepare_gb(input: &Input) {
    // Great, now we know which file we want, so access and parse it
    let file = File::open(&input.path).unwrap();
    let cart = GBCart::new(file);

    if cart.is_err() {
        return;
    }
    let header = cart.unwrap().header;

    // Prepare properties for Output... almost all fields are optional
    let licensee = match header.licensee {
        Licensee::NewLicenseeField => format!("{}", header.new_licensee.unwrap()),
        _ => format!("{}", header.licensee),
    };

    let properties = GameProperties {
        title: Some(header.title.clone()),
        description: Some(format!(
            "Cart type: {}\n\
            ROM size: {} KiB\n\
            RAM size: {} KiB\n\
            SGB capable: {}\n\
            Licensee: {licensee}\n",
            header.cart_type, header.rom_size, header.ram_size, header.sgb_flag
        )),
        //FIXME detect kind and adjust keywords
        keywords: Some(format!("Gameboy, Gameboy Color, {}", header.title)),
        ..Default::default()
    };

    let output = GameOutput {
        status: "OK".to_string(),
        properties,
    };

    println!("{}", serde_json::to_string(&output).unwrap());
}

fn prepare_gba(input: &Input) {
    // Great, now we know which file we want, so access and parse it
    let file = File::open(&input.path).unwrap();
    let cart = GBACart::new(file);

    if cart.is_err() {
        return;
    }
    let header = cart.unwrap().header;

    // Prepare properties for Output... almost all fields are optional
    let properties = GameProperties {
        title: Some(header.title.clone()),
        description: Some(format!(
            "Game code: {}\n\
            Manufacturer code: {}",
            header.game_code, header.manufacturer_code
        )),
        //publisher: Some(format!("{}", header.dev_id)),
        keywords: Some(format!("Gameboy Advance, {}", header.title)),
        ..Default::default()
    };

    let output = GameOutput {
        status: "OK".to_string(),
        properties,
    };

    println!("{}", serde_json::to_string(&output).unwrap());
}

fn prepare_nds(input: &Input) {
    // Great, now we know which file we want, so access and parse it
    let file = File::open(&input.path).unwrap();
    let cart = NDSCart::new(file);

    if cart.is_err() {
        return;
    }
    let header = cart.unwrap().header;

    // Prepare properties for Output... almost all fields are optional
    let properties = GameProperties {
        title: Some(header.title.clone()),
        description: Some(format!(
            "Game code: {}\n\
            Manufacturer code: {}\n\
            unit_code: {}\n\
            device_capacity: {}",
            header.game_code, header.manufacturer, header.unit_code, header.device_capacity
        )),
        //publisher: Some(format!("{}", header.dev_id)),
        keywords: Some(format!("Nintendo DS, {}", header.title)),
        ..Default::default()
    };

    let output = GameOutput {
        status: "OK".to_string(),
        properties,
    };

    println!("{}", serde_json::to_string(&output).unwrap());
}

fn prepare_ctr(input: &Input) {
    // Great, now we know which file we want, so access and parse it
    let file = File::open(&input.path).unwrap();
    let cart = CTRCart::new(file);

    if cart.is_err() {
        return;
    }
    let mut cart = cart.unwrap();
    let smdh = cart.read_smdh();

    if smdh.is_err() {
        return;
    }
    let smdh = smdh.unwrap();
    if smdh.is_none() {
        return;
    }
    let smdh = smdh.unwrap();

    let title = &smdh.titles[1].short_description;
    let publisher = &smdh.titles[1].publisher;

    // Prepare properties for Output... almost all fields are optional
    let properties = GameProperties {
        title: Some(title.clone()),
        description: Some(format!(
            "Game title: {title}\n\
            Publisher: {publisher}"
        )),
        publisher: Some(publisher.into()),
        keywords: Some(format!("Nintendo 3DS, {title}")),
        ..Default::default()
    };

    let output = GameOutput {
        status: "OK".to_string(),
        properties,
    };

    println!("{}", serde_json::to_string(&output).unwrap());
}

fn prepare_cia(input: &Input) {
    // Great, now we know which file we want, so access and parse it
    let file = File::open(&input.path).unwrap();
    let cia = CIA::new(file);

    if cia.is_err() {
        return;
    }
    let mut cia = cia.unwrap();
    let smdh = cia.read_smdh();

    if smdh.is_err() {
        return;
    }
    let smdh = smdh.unwrap();
    if smdh.is_none() {
        return;
    }
    let smdh = smdh.unwrap();

    let title = &smdh.titles[1].short_description;
    let publisher = &smdh.titles[1].publisher;

    // Prepare properties for Output... almost all fields are optional
    let properties = GameProperties {
        title: Some(title.clone()),
        description: Some(format!(
            "Game title: {title}\n\
            Publisher: {publisher}"
        )),
        publisher: Some(publisher.into()),
        keywords: Some(format!("Nintendo 3DS, {title}")),
        ..Default::default()
    };

    let output = GameOutput {
        status: "OK".to_string(),
        properties,
    };

    println!("{}", serde_json::to_string(&output).unwrap());
}

fn prepare_n64(input: &Input) {
    // Great, now we know which file we want, so access and parse it
    let file = File::open(&input.path).unwrap();
    let rom = N64Cart::new(file);

    if rom.is_err() {
        return;
    }
    let rom = rom.unwrap();

    let title = str::from_utf8(&rom.header.game_title).unwrap().trim();

    // Prepare properties for Output... almost all fields are optional
    let properties = GameProperties {
        title: Some(title.to_string()),
        description: Some(format!("Game title: {title}")),
        //publisher: Some(publisher.into()),
        keywords: Some(format!("Nintendo 64, {title}")),
        ..Default::default()
    };

    let output = GameOutput {
        status: "OK".to_string(),
        properties,
    };

    println!("{}", serde_json::to_string(&output).unwrap());
}

fn main() {
    // KFileMetadata sends external plugins a JSON with the fields as described in the Input
    // struct. Read these in...
    let mut stdin = io::stdin();
    let mut input_string = String::new();
    // FIXME do we want to do better error handling than just panic on error?
    stdin.read_to_string(&mut input_string).unwrap();
    let input: Input = serde_json::from_str(&input_string).unwrap();

    // Bail if we receive a mimetype we don't recognize
    match input.mimetype.as_str() {
        "audio/prs.spc" => prepare_spc(&input),
        "application/vnd.nintendo.snes.rom" => prepare_sfc(&input),
        "application/x-gamecube-rom" => prepare_gcn(&input),
        "application/prs.rvz" => prepare_gcn(&input),
        "application/x-gameboy-rom" | "application/x-gameboy-color-rom" => prepare_gb(&input),
        "application/x-gba-rom" => prepare_gba(&input),
        "application/x-nintendo-ds-rom" => prepare_nds(&input),
        "application/prs.nintendo-3ds-cia" => prepare_cia(&input),
        "application/x-nintendo-3ds-rom" => prepare_ctr(&input),
        "application/x-n64-rom" => prepare_n64(&input),
        _ => (),
    }
}
